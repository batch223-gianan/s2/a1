<?php require_once('./code.php'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php printDivisibleOfFive(); ?>
    <?php array_push($students, 'John Smith'); ?>
    <pre><?= print_r($students); ?></pre>
    <pre><p>array count:</p><?php echo count($students); ?></pre>
    <?php array_push($students, 'Jane Doe'); ?>
    <pre><?= print_r($students);?></pre>
    <pre><p>array count:</p> <?php echo count($students); ?></pre>
    <?php array_shift($students); ?>
    <pre><p>array count:</p><?php echo count($students); ?></pre>
    <pre><?= print_r($students); ?></pre>
    <pre><p>array count:</p><?php echo count($students); ?></pre>
</body>
</html>